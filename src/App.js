import React, {Component} from 'react';
import AddTaskForm from './components/AddTaskForm/AddTaskForm';
import Task from "./components/Task/Task";


import './App.css';

class App extends Component {

    state = {
        tasks: [
            {text: 'Eat', id: 1, complete: false},
            {text: 'Sleep', id: 2, complete: false},
            {text: 'Love JavaScript', id: 3, complete: false}
        ],
        value: ''
    };

    changeAddInput = (event) => {
        let newTaskValue = event.target.value;
        this.setState({value: newTaskValue});
    };

    addTask = () => {
        const id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        const newTask = {text: this.state.value, id: id, complete: false};

        const tasks = [...this.state.tasks];
        tasks.push(newTask);

        const value = '';

        this.setState({tasks, value});
    };

    removeTask = id => {
        const index = this.state.tasks.findIndex(task => task.id === id);

        const tasks = [...this.state.tasks];
        tasks.splice(index, 1);

        this.setState({tasks});
    };

    checkTask = id => {
        const index = this.state.tasks.findIndex(task => task.id === id);

        let tasks = [...this.state.tasks];
        let task = {...tasks[index]};

        task.complete = !task.complete;
        tasks[index] = task;

        this.setState({tasks});
    };


    render() {

        const tasks = this.state.tasks.map((task) => (
            <Task taskText={task.text}
                  key={task.id}
                  checkBoxChange={() => this.checkTask(task.id)}
                  click={() => this.removeTask(task.id)}/>
        ));

        return (
            <div className="App">
                <div>
                    <AddTaskForm change={event => this.changeAddInput(event)}
                                 value={this.state.value}
                                 click={() => this.addTask()}/>
                </div>
                {tasks}
            </div>
        );
    }
}

export default App;
