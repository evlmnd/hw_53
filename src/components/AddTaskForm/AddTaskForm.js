import React from 'react';

import './AddTaskForm.css';

const AddTaskForm = props =>(
    <div className="add-task-form">
        <input className="add-input" type="text" placeholder="Add new task" onChange={props.change} value={props.value}/>
        <button className="add-button" onClick={props.click}>Add</button>
    </div>
);

export default AddTaskForm;