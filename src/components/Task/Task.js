import React from 'react';

import './Task.css';

const Task = props => (
    <div className="task">
        <input className="task-check" type="checkbox" onChange={props.checkBoxChange}/>
        <p className="task-text" key={props.id}>{props.taskText}</p>
        <button className="remove-button" onClick={props.click}>x</button>
    </div>
);

export default Task;